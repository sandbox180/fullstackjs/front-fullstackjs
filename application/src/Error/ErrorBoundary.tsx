import React, { Component, ErrorInfo, ReactNode } from "react"
import { message } from "antd"

interface Props {
  children: ReactNode;
}

interface State {
  hasError: boolean;
}

class ErrorBoundary extends Component<Props, State> {
  public state: State = {
    hasError: false
  }

  public static getDerivedStateFromError (/*error: Error*/): State {
    return { hasError: true }
  }

  public componentDidCatch (/*error: Error, errorInfo: ErrorInfo*/) : void {
    message.error("Oops.. an unexpected error occurred").then()
  }

  public render () {
    return <>
      {this.props.children}
    </>
  }
}

export default ErrorBoundary
