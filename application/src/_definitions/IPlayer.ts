export interface IPlayer {
  id: string
  username: string
  blinded: boolean
  vote: -1 | 0 | 1
  teamId: string
  roomId: string
  avatar: string
}
