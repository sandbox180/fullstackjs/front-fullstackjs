export interface IRoom {
  id: string
  name: string
  started: boolean
  protected: boolean
  open: boolean
  round: number
  teamTurn: string
  avatar: string
  leader: string
  timeMaster: string
}
