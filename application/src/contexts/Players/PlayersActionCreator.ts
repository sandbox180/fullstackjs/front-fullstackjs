import { IPlayer } from "../../_definitions/IPlayer"
import { PlayerActions } from "./PlayerActions"

export const addPlayer = (payload: IPlayer) => ({
  type: PlayerActions.ADD,
  payload
})

export const removePlayer = (payload: string) => ({
  type: PlayerActions.REMOVE,
  payload
})

export const resetAllPlayers = () => ({
  type: PlayerActions.RESET
})

export const updateAllPlayers = (payload: IPlayer[]) => ({
  type: PlayerActions.ALL,
  payload
})

export const updatePlayer = (payload: IPlayer) => ({
  type: PlayerActions.UPDATE,
  payload
})
