import { IPlayer } from "../../_definitions/IPlayer"
import { PlayerActions } from "./PlayerActions"
import { Action, State } from "./PlayersContext"

type Reducer = (state: State, action: Action) => State

const PlayerReducer: Reducer = (prevState, action) => {

  switch (action.type) {

  case PlayerActions.ADD: {

    const newPlayer = action.payload as IPlayer
    if (prevState.some((player) => player.id === newPlayer.id))
      return prevState
    return [...prevState, newPlayer]

  }
  case PlayerActions.REMOVE: {

    const removedPlayerId = action.payload as string
    return prevState.filter((player) => player.id !== removedPlayerId)

  }
  case PlayerActions.UPDATE: {

    const updPlayer = action.payload as IPlayer
    const prevStateCopy = [...prevState]
    const index = prevState.findIndex((player) => player.id === updPlayer.id)
    if (index !== -1)
      prevStateCopy[index] = updPlayer
    return prevStateCopy

  }
  case PlayerActions.ALL:
    return action.payload as IPlayer[]
  case PlayerActions.RESET:
    return []
  default:
    throw new Error(`Unhandled action type: ${action.type}`)

  }

}

export { PlayerReducer }
