import React, { FC, ReactNode } from "react"
import { RoomProvider } from "./Room/RoomContext"
import { PlayersProvider } from "./Players/PlayersContext"

type GameProps = { children?: ReactNode }

const GameProvider: FC<GameProps> = ({ children }) => {

  return (
    <RoomProvider>
      <PlayersProvider>
        {children}
      </PlayersProvider>
    </RoomProvider>

  )

}

export { GameProvider }
