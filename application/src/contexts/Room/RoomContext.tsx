import React, { createContext, Dispatch, FC, ReactNode, SetStateAction, useContext, useMemo, useState } from "react"
import { IRoom } from "../../_definitions/IRoom"

type IRoomContext = {
  room: IRoom
  setRoom: Dispatch<SetStateAction<IRoom>> & Dispatch<IRoom>
}
type Props = { children?: ReactNode }

const RoomContext = createContext<IRoomContext | null>(null)

const roomTemplate: IRoom = {
  id: "",
  name: "",
  avatar: "",
  protected: false,
  started: false,
  open: true,
  teamTurn: "",
  leader: "",
  timeMaster: "",
  round: 0
}

const RoomProvider: FC<Props> = ({ children }) => {

  const [room, setRoom] = useState<IRoom>(roomTemplate)

  const value: IRoomContext = useMemo(() => ({ room, setRoom }), [room])

  return <RoomContext.Provider value={value}>{children}</RoomContext.Provider>

}

function useRoom () {

  const roomContext = useContext(RoomContext)
  if (!roomContext)

    throw new Error("useRoom must be used within a RoomProvider")

  return roomContext as IRoomContext

}

export { roomTemplate, RoomProvider, useRoom }
