import React from "react"
import { Outlet } from "react-router-dom"

function ChatOutlet () {
  return (
    <div>
      <Outlet />
    </div>

  )

}

export default ChatOutlet
