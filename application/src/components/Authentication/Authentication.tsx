import React, { ChangeEvent, useCallback, useState } from "react"
import { Button, Card, Col, Input, Layout, Row } from "antd"
import { UserOutlined } from "@ant-design/icons"
import { useNavigate } from "react-router-dom"

function Authentication () {

  const navigate = useNavigate()
  const [username, setUsername] = useState<string>()

  const handleUsername = (e: ChangeEvent<HTMLInputElement>) => {
    setUsername(e.target.value)
  }

  const handleNavigage = useCallback(
    () => {
      navigate("../childTwo", { replace: false })
    },
    [navigate]
  )

  const colSpan = { xs: 24, sm: 22, md: 18, lg: 14, xl: 12 }

  return (
    <Layout>
      <Row>
        <Col span={6} style={{ margin: "auto", minWidth: "16rem" }}>
          <Card type='inner' title='User name' bordered={false}>
            <Input
              value={username}
              onChange={handleUsername}
              size='large'
              placeholder='Enter your username'
              prefix={<UserOutlined className='site-form-item-icon' />}
            />
          </Card>
        </Col>
      </Row>

      <Row style={{ marginTop: "2rem" }}>
        <Col
          {...colSpan}
          offset={6}
          style={{ margin: "auto", minWidth: "16rem" }}
        >
        </Col>
      </Row>
      <Button type="primary" style={{ width: "fit-content" }} onClick={handleNavigage}> Navigate to child two</Button>
    </Layout>
  )

}

export default Authentication
