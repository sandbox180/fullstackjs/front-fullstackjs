import React from "react"
import { Navigate, Route, Routes as Switch } from "react-router-dom"
import { Card } from "antd"
import ChatOutlet from "./components/ChatOutlet/ChatOutlet"
import Authentication from "./components/Authentication/Authentication"
import ChatBox from "./components/ChatBox/ChatBox"

function Router () {

  return (
    <Switch>
      <Route path='authentication' element={<Authentication />} />
      <Route path='user' element={<ChatOutlet /> }>
        <Route index element={<Navigate to='childOne' replace />} />
        <Route path='chatbox' element={<ChatBox />} />
      </Route>
      <Route path='*' element={<Navigate to='user/chatbox' replace />} />
    </Switch>
  )

}

export { Router }
