module.exports = {
    "env": {
        "browser": true,
        "es2021": true,
        "jest": true
    },
    "settings": {
        "import/resolver": {
            "typescript": {}
        },
        "react": {
            "version": "detect",
        },
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:react-hooks/recommended"
    ],
    "plugins": [
        "react",
        "@typescript-eslint",
        "react-hooks"
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "rules": {
        /*---------- MY RULES OPTIMISATION ------------- */
        "react/prop-types": "off",
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "warn",
        "react/no-unescaped-entities": "off",


        '@typescript-eslint/no-inferrable-types': 0,
        'import/no-unresolved': 0,
        'import/prefer-default-export': 0,
        '@typescript-eslint/semi': [2, 'never'],
        "indent": ["error", 2],
        'max-len': [2, 120, { ignoreComments: true, ignoreStrings: true }],
        'no-unexpected-multiline': 2,
        'no-underscore-dangle': 0,
        'no-continue': 0,
        'brace-style': ["error", "stroustrup"],
        'quotes': ["error", "double"],
        "comma-dangle": ["error", "never"],
        "space-in-parens": ["error", "never"],
        "comma-spacing": ["error", { "before": false, "after": true }],
        "curly": [2, "multi-or-nest", "consistent"],
        "nonblock-statement-body-position": [2, "below"],
        "space-infix-ops": "error",
        "object-curly-spacing": ["error", "always"],
        "keyword-spacing": ["error", { "before": true, "after": true }],
        "key-spacing": ["error", { "beforeColon": false, "afterColon": true }],
        "array-bracket-spacing": ["error", "never"],
        "space-before-function-paren": ["error", {
            "anonymous": "always",
            "named": "always",
            "asyncArrow": "always"
        }],
        "no-trailing-spaces": ["error", { "skipBlankLines": true }],
        "no-multiple-empty-lines": ["error", { "max": 1, "maxEOF": 0, "maxBOF": 0 }],
        "no-multi-spaces": "error"

    }
}
