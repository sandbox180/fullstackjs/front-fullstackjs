import { GoogleSearch } from '../page-objects/duckduckgo-search.page';
const search = new GoogleSearch();

describe('Google Navigation', () => {
  it('Google Search',() => {
    cy.visit({
      url: 'https://duckduckgo.com',
      method: 'GET',
    });

    search.duckduckgoSearchBar().type('Something');
    search.duckduckgoSearchBtn().click({ force: true });

    search.searchResultH2().should('be.visible');
    search.searchResultH1().should('be.visible');
  });
});
