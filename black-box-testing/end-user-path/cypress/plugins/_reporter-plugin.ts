import { beforeRunHook, afterRunHook } from 'cypress-mochawesome-reporter/lib';
import {execSync} from 'child_process'

export default (on) => {
  on('before:run', async (details) => {
    console.log('override before:run');
    await beforeRunHook(details);
  });

  on('after:run', async () => {
    console.log('override after:run');
    execSync(`npm run ci:report:junit:merge`)
    await afterRunHook();
  });
};
