ifeq (build,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif




#============INSERT VALUE HERE ================
APP_PATH = ./application
DOCKER_REGISTRY=registry.gitlab.com/sandbox180/fullstackjs/front-fullstackjs
API_NAME=api-fullstackjs
#=============================================



#--------Init NPM (Husky & Application modules) ------------

init:
	npm i
	npm --prefix $(APP_PATH) install



#--------Gitver ------------


GITVER_IMAGE = gittools/gitversion:5.10.1-alpine.3.14-6.0
CURRENT_DIR = $$(pwd)
CONTAINER_DIR = /repo
COMMIT_REF_NAME = master

gitver:
	docker run --rm -v "$(CURRENT_DIR):$(CONTAINER_DIR)" $(GITVER_IMAGE) $(CONTAINER_DIR) | grep "FullSemVer"
gitver-full:
	docker run --rm -v "$(CURRENT_DIR):$(CONTAINER_DIR)" $(GITVER_IMAGE) $(CONTAINER_DIR)


#----------- Build -------------


IMAGE_NAME=$(DOCKER_REGISTRY)

.PHONY: build
.ONESHELL:
build:
	docker build --rm --build-arg API_NAME=$(API_NAME) -t $(IMAGE_NAME):latest .
	[ ! -z  $(RUN_ARGS) ] && docker tag $(IMAGE_NAME):latest $(IMAGE_NAME):$(RUN_ARGS)
	docker push --all-tags $(IMAGE_NAME)
